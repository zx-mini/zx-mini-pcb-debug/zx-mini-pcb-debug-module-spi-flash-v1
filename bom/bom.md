|       | References	  | Value	        | Footprint	                  | Quantity |
|-------|---------------|---------------|-----------------------------|----------|
| 1			| C1	| 0.1uF	| C_0805_2012Metric	| 1
| 2			| C2	| 4.7uF	| C_0805_2012Metric	| 1
| 3			| R1, R3, R4, R5, R6	| 4.7k	| R_0805_2012Metric	| 5
| 4			| R2	| 470	| R_0805_2012Metric	| 1
| 5			| D1	| TO-1608BC-MRE	| LED_0603_1608Metric	| 1
| 6			| U1	| W25Q128JVS	| SOIC-8_5.23x5.23mm_P1.27mm	| 1
| 7			| J5, J6	| PLS-3	| PinHeader_1x03_P2.54mm_Vertical	| 2
| 8			| J3, J4	| PLS-4	| PinHeader_1x04_P2.54mm_Vertical	| 2
| 9			| J1, J2	| PBS-4	| PinSocket_1x04_P2.54mm_Vertical	| 2
